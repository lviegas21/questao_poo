package prova_poo;

import java.util.Date;


//classe heran�a que est� herdando qualidades da classe item
public class Book extends Item{
	
	
	//variaveis privadas para utilizar o conceito de encapsulamento
	private String author;
	private String edition;
	private String volume;
	
	//Construtor pegando os itens do objeto criado na classe executavel
	public Book(String title, String publisher, Date yearpublished, String isbn, double price, String author, String edition, String volume) {
		super(title, publisher, yearpublished, isbn, price);
		
		
		this.author = author;
		this.edition = edition;
		this.volume = volume;
	}
	
	//metodos acessores getters e setters
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	
	//fun��o display, para imprimir os atributos.
	
	public void Display() {
		System.out.println("=== Os itens do livro ===");
	    System.out.println("Title: " + getTitle());	
	    System.out.println("Publisher: " + getPublisher());	
	    System.out.println("Published: " + getYearpublished());	
	    System.out.println("ISBN: " + getIsbn());	
	    System.out.println("Price: " + getPrice());	
	    System.out.println("Author: " + this.author);	
	    System.out.println("Edition: " + this.edition);
	    System.out.println("Volume: " + this.volume);	
	}
}
