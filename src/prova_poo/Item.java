package prova_poo;
import java.util.Date;



//variaveis declaradas
public class Item {
	private String title;
	private String publisher;
	private Date yearpublished;
	private String isbn;
	private double price;
	
	//construtor recebendo valores da classe executavel
	public Item(String title, String publisher, Date yearpublished, String isbn, double price) {
		this.title = title;
		this.publisher = publisher;
		this.yearpublished = yearpublished;
		this.isbn = isbn;
		this.price = price;
	}
	
	//fun��o display da classe Item
	public void Display() {
		System.out.println("==== Os itens ====");
	    System.out.println("Title: " + this.title);
	    System.out.println("publisher: " + this.publisher);	
	    System.out.println("yerapublished: " + this.yearpublished);	
	    System.out.println("isbn: " + this.isbn);	
	    System.out.println("price: " + this.price);	
	}
	
	
	//metodos acessores
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public Date getYearpublished() {
		return yearpublished;
	}
	public void setYearpublished(Date yearpublished) {
		this.yearpublished = yearpublished;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
